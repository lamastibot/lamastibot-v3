"""
MIT License

Copyright (c) 2022-now Sakiut

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from discord import Member, TextChannel, Message, ApplicationContext
from discord.commands import Option
from discord.ext import commands

from datetime import timedelta

from main import Bot
from utils import checks
from utils.common import slash_command, parse_duration


class Moderation(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot

    @slash_command(default_permission=False)
    @checks.is_owner()
    @checks.mod_or_permissions()
    async def ban(self, ctx: ApplicationContext,
                  user: Option(Member, "Utilisateur concerné", required=True),
                  reason: Option(str, "Raison du bannissement", required=True),
                  days: Option(int, "Nombre de jours de messages à supprimer", default=1)):
        """Bannir un utilisateur du serveur"""
        await ctx.guild.ban(user, reason=reason, delete_message_days=days)
        await ctx.respond(f"Utilisateur {user} banni avec succès", ephemeral=True)

    @slash_command(default_permission=False)
    @checks.is_owner()
    @checks.mod_or_permissions()
    async def hban(self, ctx: ApplicationContext,
                   user_id: Option(int, "ID de l'utilisateur", required=True)):
        """Bannir un utilisateur à partir de son ID"""
        await self.bot.http.ban(user_id, ctx.guild.id, 0)
        await ctx.respond(f"Utilisateur d'ID {user_id} banni avec succès", ephemeral=True)

    @slash_command(default_permission=False)
    @checks.is_owner()
    @checks.mod_or_permissions()
    async def kick(self, ctx: ApplicationContext,
                   user: Option(Member, "Utilisateur concerné", required=True),
                   reason: Option(str, "Raison de l'expulsion", required=True)):
        """Expulser un utilisateur du serveur"""
        await ctx.guild.kick(user, reason=reason)
        await ctx.respond(f"Utilisateur {user} expulsé avec succès", ephemeral=True)

    @slash_command(default_permissions=False)
    @checks.is_owner()
    @checks.mod_or_permissions()
    async def mute(self, ctx: ApplicationContext,
                   user: Option(Member, "Utilisateur concerné", required=True),
                   duration: Option(str, "Durée du mute", required=True),
                   reason: Option(str, "Raison du mute", required=True)):
        """Mute temporaire d'un utilisateur sur le salon courant"""
        duration = timedelta(seconds=parse_duration(duration))
        await user.timeout_for(duration, reason=reason)
        await ctx.respond(f"Utilisateur {user} muté avec succès pour {duration}", ephemeral=True)

    @slash_command(default_permission=False)
    @checks.is_owner()
    @checks.mod_or_permissions()
    async def unmute(self, ctx: ApplicationContext,
                     user: Option(Member, "Utilisateur concerné", required=True),
                     reason: Option(str, "Raison du mute", required=False)):
        """Unmute un utilisateur sur le salon courant"""
        await user.remove_timeout(reason=reason if reason else None)
        await ctx.respond(f"Utilisateur {user} unmuté avec succès", ephemeral=True)

    @slash_command(default_permission=False)
    @checks.is_owner()
    @checks.mod_or_permissions()
    async def clear(self, ctx: ApplicationContext,
                    user: Option(Member, "Utilisateur concerné", required=False),
                    amount: Option(int, "Nombre de messages à supprimer", required=False, max_value=100),
                    all_channels: Option(bool, "Appliquer le clear à tous les salons", required=False)):
        """Supprimer un certain nombre de messages simultanément"""
        if amount is None:
            amount = 10 if user is None else 100

        async def clear_channel(chan: TextChannel) -> list[Message]:
            if user is not None:
                return await chan.purge(limit=amount, check=lambda m: m.author == user)
            else:
                return await chan.purge(limit=amount)

        if all_channels:
            deleted = []
            for channel in ctx.guild.text_channels:
                deleted += await clear_channel(channel)
        else:
            deleted = await clear_channel(ctx.channel)

        if user is not None:
            await ctx.respond(f"{len(deleted)} messages de {user} supprimés", ephemeral=True)
        else:
            await ctx.respond(f"{len(deleted)} messages supprimés", ephemeral=True)
