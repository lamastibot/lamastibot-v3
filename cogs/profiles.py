"""
MIT License

Copyright (c) 2022-now Sakiut

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import discord
from discord import ApplicationContext, Member
from discord.commands import Option
from discord.ext import commands

from main import Bot
from utils import checks
from utils.common import slash_command
from models import profiles


class Profile(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot

    @slash_command(default_permission=False)
    async def profile(self, ctx: ApplicationContext,
                      user: Option(Member, "Utilisateur concerné", required=False)):
        """Afficher le profil d'un utilisateur"""
        if user is None:
            user = ctx.author

        total_score = 0
        scores: list[profiles.ChannelScore] = await profiles.ChannelScore.objects.filter(user_id=user.id).all()
        for score in scores:
            total_score += score.score

        embed = discord.Embed()
        embed.title = f"{user}"
        embed.add_field(name="Current month total score", value=str(total_score))
        await ctx.respond(embed=embed, ephemeral=True)
