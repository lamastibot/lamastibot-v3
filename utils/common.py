"""
MIT License

Copyright (c) 2022-now Sakiut

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from discord.commands import slash_command as sc

import toml
import re


with open('config.toml') as f:
    config = toml.load(f)
    f.close()


def slash_command(**kwargs):
    return sc(guild_ids=[config['discord']['guild_id']], **kwargs)


def parse_duration(duration: str) -> int:
    seconds = 0
    days = re.findall(r'(\d+)\s*d', duration)
    hours = re.findall(r'(\d+)\s*h', duration)
    minutes = re.findall(r'(\d+)\s*m', duration)
    secs = re.findall(r'(\d+)\s*s', duration)
    seconds += int(days[0])*86400 if days else 0
    seconds += int(hours[0])*3600 if hours else 0
    seconds += int(minutes[0])*60 if minutes else 0
    seconds += int(secs[0]) if secs else 0
    return seconds
