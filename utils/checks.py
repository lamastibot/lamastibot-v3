"""
MIT License

Copyright (c) 2022-now Sakiut

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import discord.utils
from discord.ext import commands

from utils.common import config as conf


def is_owner_check(ctx):
    return ctx.author.id == conf['discord']['owner_id']


def is_owner():
    return commands.check(lambda ctx: is_owner_check(ctx))


def is_channel(channel_id):
    def predicate(ctx):
        return ctx.channel.id == channel_id

    return commands.check(predicate)


def check_permissions(ctx, perms):
    if is_owner_check(ctx):
        return True

    ch = ctx.channel
    author = ctx.author
    resolved = ch.permissions_for(author)
    return all(getattr(resolved, name, None) == value for name, value in perms.items())


def role_or_permissions(ctx, check, **perms):
    if check_permissions(ctx, perms):
        return True

    ch = ctx.channel
    author = ctx.author
    if isinstance(ch, (discord.DMChannel, discord.GroupChannel)):
        return False  # can't have roles in PMs

    role = discord.utils.find(check, author.roles)
    if role is None:
        raise commands.CommandError("You need a special role to do this!")
    return True


def mod_or_permissions():
    def predicate(ctx):
        return role_or_permissions(ctx, lambda r: r.id == conf['discord']['roles']['mods'], manage_roles=True)

    return commands.check(predicate)


def admin_or_permissions():
    def predicate(ctx):
        return role_or_permissions(ctx, lambda r: r.id == conf['discord']['roles']['admins'], administrator=True)

    return commands.check(predicate)


def nsfw_channel():
    # noinspection PyUnresolvedReferences
    def predicate(ctx):
        if not (isinstance(ctx.channel,
                           (discord.DMChannel, discord.GroupChannel)) or "nsfw" in ctx.channel.name.casefold()):
            raise ChannelError("This command can only be used in `nsfw` channels!")
        return True

    return commands.check(predicate)
