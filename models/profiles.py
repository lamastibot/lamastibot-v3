"""
MIT License

Copyright (c) 2022-now Sakiut

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import ormar
import uuid
import datetime

from models import database, metadata


# === Models

class Profile(ormar.Model):
    discord_uid: int = ormar.BigInteger(primary_key=True)
    twitch_uid: int = ormar.BigInteger(unique=True, nullable=True, sql_nullable=True)

    discord_name: str = ormar.String(max_length=127)
    twitch_name: str = ormar.String(max_length=127, nullable=True, sql_nullable=True)

    time_master: bool = ormar.Boolean(default=False)

    class Meta:
        tablename = "bot_profile"
        database = database
        metadata = metadata


class ChannelScore(ormar.Model):
    id: str = ormar.UUID(primary_key=True, default=uuid.uuid4)
    user_id: Profile = ormar.ForeignKey(Profile)

    channel_id: int = ormar.BigInteger()
    channel_name: str = ormar.String(max_length=32)

    month: int = ormar.SmallInteger()
    year: int = ormar.Integer()

    score: int = ormar.BigInteger(default=0)
    messages_from_last_score: int = ormar.Integer(default=0)
    last_score_date: datetime.datetime = ormar.DateTime()

    class Meta:
        tablename = "bot_channelscore"
        database = database
        metadata = metadata


# === Methods

async def update_score(channel_score: ChannelScore, amount: int) -> None:
    await (await ChannelScore.objects.get(id=channel_score.id)).update(
            score=channel_score.score + amount,
            messages_from_last_score=0,
            last_score_date=datetime.datetime.now()
        )


async def update_message_count(channel_score: ChannelScore, amount: int = 1) -> None:
    await (await ChannelScore.objects.get(id=channel_score.id)).update(
            messages_from_last_score=channel_score.messages_from_last_score + amount,
        )
