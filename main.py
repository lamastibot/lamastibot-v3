"""
MIT License

Copyright (c) 2022-now Sakiut

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import discord
from discord.ext import commands

import random
from datetime import datetime, date, timedelta
from colorama import init, Fore, Style

from models import database
from models import profiles as pmods
from utils.common import config as conf


class Bot(commands.AutoShardedBot):

    def __init__(self, config: dict, *args, **kwargs):
        super().__init__(*args, activity=discord.Game(name="Corobizar.com | /help"), **kwargs)

        # Global-scale variables
        self.config: dict = config
        self.uptime: datetime = datetime.now()
        self.date: date = date.today()
        self.db = database

        # Cogs
        from cogs.moderation import Moderation
        from cogs.profiles import Profile
        self.add_cog(Moderation(self))
        self.add_cog(Profile(self))

    async def on_connect(self):
        print(f"Connecting database...{Style.RESET_ALL}", end="\r")
        await self.db.connect()
        print(f"Connecting database... {Fore.LIGHTGREEN_EX}OK{Style.RESET_ALL}")
        await super().on_connect()

    async def on_ready(self):
        print(f"Logged in as {Fore.CYAN}{self.user}{Style.RESET_ALL} ({Fore.CYAN}{self.user.id}{Style.RESET_ALL})")

    async def on_message(self, message: discord.Message):
        ctx: commands.Context = await self.get_context(message)

        print(f"New message: {Fore.LIGHTYELLOW_EX}{ctx.message.content}{Style.RESET_ALL} "
              f"by {Fore.CYAN}{ctx.author}{Style.RESET_ALL} "
              f"in {Fore.CYAN}#{ctx.channel}{Style.RESET_ALL}")

        if ctx.author.bot or ctx.message.guild is None:
            print(f"\t{Fore.RED}{ctx.author}{Style.RESET_ALL}: Ignored")
            return

        await pmods.Profile.objects.get_or_create(discord_uid=ctx.author.id,
                                                  _defaults={"discord_name": str(ctx.author)})
        current_score, is_new = await pmods.ChannelScore.objects.get_or_create(
            user_id=ctx.author.id,
            channel_id=ctx.channel.id,
            month=date.today().month, year=date.today().year,
            _defaults={
                "channel_name": str(ctx.channel),
                "last_score_date": datetime.now() - timedelta(days=1)
            }
        )

        # Ignoring palindromes, links and emojis
        if ctx.message.content[1:] == ctx.message.content[:1] or \
                len(ctx.message.content) < 3 or \
                ctx.message.content.lower().startswith("http://") or \
                ctx.message.content.lower().startswith("https://") or \
                (ctx.message.content.lower().startswith("<:") and ctx.message.content.lower().endswith(">")):
            print(f"\t{Fore.RED}{ctx.author}{Style.RESET_ALL}: Ignored")
            return

        if current_score.last_score_date.date() < ctx.message.created_at.date():
            await pmods.update_score(current_score, 100)
            print(f"\t{Fore.CYAN}{ctx.author}{Style.RESET_ALL}: Given daily bonus (100)")

        elif abs((ctx.message.created_at - current_score.last_score_date)) > timedelta(minutes=random.randint(4, 8)) \
                and len(ctx.message.content) >= 3 \
                and current_score.messages_from_last_score >= random.randint(1, 2):
            await pmods.update_score(current_score, 15)
            print(f"\t{Fore.CYAN}{ctx.author}{Style.RESET_ALL}: Given standard score amount (15)")

        else:
            await pmods.update_message_count(current_score)
            print(f"\t{Fore.CYAN}{ctx.author}{Style.RESET_ALL}: Increased message amount "
                  f"({current_score.messages_from_last_score} + 1)")

        await super().on_message(message)


if __name__ == "__main__":
    init(autoreset=True)
    intents = discord.Intents.all()
    bot = Bot(config=conf, intents=intents, owner_id=conf['discord']['owner_id'])
    print(f"{Fore.LIGHTBLACK_EX}Connecting...{Style.RESET_ALL}")
    bot.run(token=conf['tokens']['discord'])
